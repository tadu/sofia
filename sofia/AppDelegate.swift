

//
//  AppDelegate.swift
//  sofia
//
//  Created by atauri on 3/8/17.
//  Copyright © 2017 atauri. All rights reserved.
//

import UIKit
import Firebase
import UserNotifications
import FirebaseInstanceID
import FirebaseMessaging


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate, MessagingDelegate {
    
    
    var window: UIWindow?
    let ini=Settings()


    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        
        print("\n------------------- notificaciones  PEDR PEDRMISO -------------------")
        if #available(iOS 10.0, *) {
            
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            
            // For iOS 10 data message (sent via FCM
            Messaging.messaging().delegate = self
            
            
        } else {
            print("--> configurar en 9.3")
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        
        print("---> FirebaseApp.configure()\n")
        FirebaseApp.configure()
        print("?\n")
        NotificationCenter.default.addObserver(self, selector: #selector(tokenRefreshNotification(_:)), name: NSNotification.Name.InstanceIDTokenRefresh, object: nil)
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // COSITAS DEL PUSH
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    func application(application: UIApplication, didRegisterUserNotificationSettings notificationSettings: UIUserNotificationSettings) {
        print("\n\n HOLA???")
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        //ok en 10.3
        
        print("\n\n ESTOY LISTO!\n")
      
        //guardar el token para que lo vea el viewcontroller
        if let t=InstanceID.instanceID().token(){ self.ini.token=t }

        print("me subscribo a topic........\n")
        
    }
    
    
    func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError!) {
        print("\n\n---> ERROR al REGISTRAR: \(error)")
    }
    
    
    //para corregir el error DE COMPILACION (?)
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
    }
    func application(received remoteMessage: MessagingRemoteMessage) {
        // Aqui no entra pero casca si no está...
        print(remoteMessage.appData)
        print("______________________")
    }
    
    
    //------------------------------------------------------------------------------------------------------------------------
    //  RECIBO LA NOTIFICACION SI LA APP ESTA EN PRIMER PLANO
    //------------------------------------------------------------------------------------------------------------------------
    
    
    // Push notification received (ok en 9.3 pero NO en 10.3)
    func application(_ application: UIApplication, didReceiveRemoteNotification data: [AnyHashable : Any]) {
        
        // Print notification payload data
        print("***> Push notification received:")
        let aps = data[AnyHashable("aps")] as? NSDictionary
        let alert = aps?["alert"] as? NSDictionary
        let body = alert?["body"] as? String
        let title = alert?["title"] as? String
        
        
        let topWindow = UIWindow(frame: UIScreen.main.bounds)
        topWindow.rootViewController = UIViewController()
        topWindow.windowLevel = UIWindowLevelAlert + 1
        let al = UIAlertController(title: title, message: body, preferredStyle: .alert)
        al.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "confirm"), style: .cancel, handler: {(_ action: UIAlertAction) -> Void in
            topWindow.isHidden = true
        }))
        topWindow.makeKeyAndVisible()
        topWindow.rootViewController?.present(al, animated: true, completion: { _ in })
        
    }
    //--------------
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
    {
        
       completionHandler(UNNotificationPresentationOptions.alert) //esto hace que salga la notificación en la barra
       print("Yeah !!!", notification)
        
    }
    
    //------------------------------------------------------------------------------------------------------------------------
    //  NUEVO TOKEN    
    //------------------------------------------------------------------------------------------------------------------------
    
    func tokenRefreshNotification(_ notification: Notification) {
        /*
         aqui solo entra en la primera ejecución, cuando el toke cambia !!
         */
        
        print("\n\n********** TOKEN")
        ini.token = InstanceID.instanceID().token()!
   
        if((ini.token) != nil) {
            
            print(ini.token)
            
            var when = DispatchTime.now() + 3 // change 2 to desired number of seconds
            DispatchQueue.main.asyncAfter(deadline: when) {
                print("\n OnTGS!!")
                Messaging.messaging().subscribe(toTopic: self.ini.topic )
                
                // recargar la pag
                //guardar el token para que lo vea el viewcontroller
                if let t=InstanceID.instanceID().token(){ self.ini.token=t }

                
            }
            
            when = DispatchTime.now() + 4 //  secons
            DispatchQueue.main.asyncAfter(deadline: when) {
                print("\n TEST!!")
                Messaging.messaging().subscribe(toTopic: "TEST" )
                
                let topWindow = UIWindow(frame: UIScreen.main.bounds)
                topWindow.rootViewController = UIViewController()
                topWindow.windowLevel = UIWindowLevelAlert + 1
                let al = UIAlertController(title: "", message: "Suscrito al canal OnTGS", preferredStyle: .alert)
                al.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "confirm"), style: .cancel, handler: {(_ action: UIAlertAction) -> Void in
                    topWindow.isHidden = true
                }))
                topWindow.makeKeyAndVisible()
                topWindow.rootViewController?.present(al, animated: true, completion: { _ in })
                
                
            }
        }else{
        
            //no hay token
            let topWindow = UIWindow(frame: UIScreen.main.bounds)
            topWindow.rootViewController = UIViewController()
            topWindow.windowLevel = UIWindowLevelAlert + 1
            let al = UIAlertController(title: ":-((", message: "error de token", preferredStyle: .alert)
            al.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "confirm"), style: .cancel, handler: {(_ action: UIAlertAction) -> Void in
                topWindow.isHidden = true
            }))
            topWindow.makeKeyAndVisible()
            topWindow.rootViewController?.present(al, animated: true, completion: { _ in })
        }
    }
    
    
}
