//
//  ViewController.swift
//  pushdilover
//
//  Created by atauri on 20/7/17.
//  Copyright © 2017 atauri. All rights reserved.
//

import UIKit
import FirebaseInstanceID
import Darwin



class ViewController: UIViewController {
    
    @IBOutlet weak var vistaWeb: UIWebView!
    var URL_TELEFONICA = ""
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var gameTimer: Timer!
    //let id = UIDevice.current.identifierForVendor!.uuidString
    
    let myActivityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.white)
   
    override func viewDidLoad() {


        super.viewDidLoad()
        self.automaticallyAdjustsScrollViewInsets = false //quta la barra gris gaurrindonga
        
        vistaWeb.loadHTMLString(appDelegate.ini.htmlEspera, baseURL: nil)
        
        gameTimer = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(runTimedCode), userInfo: nil, repeats: true)
 
        print("\nviewDidLoad()!!!!!!!")
        
        //esconder la barra de navegacion
        self.navigationController?.isNavigationBarHidden = true;
  
        
        //Create Activity Indicator
        myActivityIndicator.center = view.center
        myActivityIndicator.startAnimating()
        view.addSubview(myActivityIndicator)
        
    
    }
    func runTimedCode() {
        
        print ("\nCheck token")
        guard let token = UIDevice.current.identifierForVendor?.uuidString

            else {
                print ("norl")
                return
        }
        
        print("\n tengo token :-)) voy a cargar la pagina \n")
        URL_TELEFONICA=appDelegate.ini.urlPpal+token
        recargarPag()
        gameTimer.invalidate()
       
        
        
        
    }
    //------------------------------------------------------------------------------------------------------------------------
    //  RECIBO LA NOTIFICACION SI LA APP ESTA EN PRIMER PLANO
    //------------------------------------------------------------------------------------------------------------------------
    func recargarPag() {

        myActivityIndicator.stopAnimating()
        
        print("\n***cargar web:\n ", URL_TELEFONICA)
        let url = URL (string: URL_TELEFONICA)
        let requestObj = URLRequest(url: url!)
        vistaWeb.loadRequest(requestObj)
        
            
        }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func mostrarSettings(_ sender: UIBarButtonItem) {
        print("Mostrar settings")
    }
    
    
    
}

