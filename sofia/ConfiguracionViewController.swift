//
//  ConfiguracionViewController.swift
//  pushdilover
//
//  Created by atauri on 24/7/17.
//  Copyright © 2017 atauri. All rights reserved.
//

import UIKit

import FirebaseInstanceID
import FirebaseMessaging


class ConfiguracionViewController: UIViewController {
    
    // SETINGS
    @IBOutlet weak var miToken: UITextView!
    @IBOutlet weak var urlInicial: UITextView!
    @IBOutlet weak var firebaseId: UITextField!
    @IBOutlet weak var sendPhp: UITextField!
    @IBOutlet weak var topic: UITextField!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        //cargar los settings
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        miToken.text=appDelegate.ini.token
        urlInicial.text=appDelegate.ini.urlPpal
        firebaseId.text=appDelegate.ini.firebaseId
        sendPhp.text=appDelegate.ini.urlPhp
        topic.text=appDelegate.ini.topic
        
        //------
        print("suscribirse")
        Messaging.messaging().subscribe(toTopic: "OnTGS" )
        Messaging.messaging().subscribe(toTopic: "TEST" )
        
        print("\n TOKEN \n")
        print(InstanceID.instanceID().token()!)
        
        print("\n Auth:\n")
        print(appDelegate.ini.firebaseId)
        
        appDelegate.ini.token=InstanceID.instanceID().token()!
        miToken.text=appDelegate.ini.token
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func mandarNotificacion(_ sender: Any) {
  
        //Llama a un php para que se conecte al firebase y le mande la notificación
        print("PUSH ME!")
        let req = NSMutableURLRequest(url: NSURL(string: "http://apilink.net/phps/push.php")! as URL)
        req.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: req as URLRequest) { data, response, error in
            if error != nil {
                //Your HTTP request failed.
                print(error?.localizedDescription)
            } else {
                //Your HTTP request succeeded
                print("OK!!!!")
            }
            }.resume()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
